/* -------------------------------------------------------------------------
  Copyright (C) 2016-2017  Miguel Carcamo, Pablo Roman, Simon Casassus,
  Victor Moral, Fernando Rannou - miguel.carcamo@usach.cl

  This program includes Numerical Recipes (NR) based routines whose
  copyright is held by the NR authors. If NR routines are included,
  you are required to comply with the licensing set forth there.

	Part of the program also relies on an an ANSI C library for multi-stream
	random number generation from the related Prentice-Hall textbook
	Discrete-Event Simulation: A First Course by Steve Park and Larry Leemis,
  for more information please contact leemis@math.wm.edu

  For the original parts of this code, the following license applies:

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
* -------------------------------------------------------------------------
*/
#include "functions.cuh"


extern long M, N;
extern int numVisibilities, iterations, iterthreadsVectorNN, blocksVectorNN, nopositivity, crpix1, crpix2, \
status_mod_in, verbose_flag, xcorr_flag, clip_flag, nsamples, nfields, nstokes, num_gpus, selected, iter, t_telescope, multigpu, firstgpu, reg_term;

extern cufftHandle plan1GPU;
extern cufftComplex *device_I, *device_V, *device_fg_image, *device_image, *device_noise_image;

extern float *device_dphi, *device_chi2, *device_S, *device_dchi2_total, *device_dS;
extern float difmap_noise, fg_scale, DELTAX, DELTAY, deltau, deltav, noise_cut, MINPIX, \
minpix, lambda, ftol, random_probability, final_chi2, final_S;

extern dim3 threadsPerBlockNN, numBlocksNN;

extern float beam_noise, beam_bmaj, beam_bmin, b_noise_aux, beam_fwhm, beam_freq, beam_cutoff;
extern double ra, dec;

extern freqData data;

extern char* mempath, *out_image;

extern fitsfile *mod_in;

extern Field *fields;

__host__ void goToError()
{
  if(num_gpus > 1){
    for(int i=firstgpu+1; i<firstgpu + num_gpus; i++){
          cudaSetDevice(firstgpu);
          cudaDeviceDisablePeerAccess(i);
          cudaSetDevice(i);
          cudaDeviceDisablePeerAccess(firstgpu);
    }

    for(int i=0; i<num_gpus; i++ ){
          cudaSetDevice((i%num_gpus) + firstgpu);
          cudaDeviceReset();
    }
  }

  printf("An error has ocurred, exiting\n");
  exit(0);

}

__host__ void init_beam(int telescope)
{
  switch(telescope) {
  case 1:
    beam_fwhm = 33.0*RPARCM;   /* radians CBI2 */
    beam_freq = 30.0;          /* GHz */
    beam_cutoff = 90.0*RPARCM; /* radians */
    break;
  case 2:
    beam_fwhm = (8.4220/60)*RPARCM;   /* radians ALMA */
    beam_freq = 691.4;          /* GHz */
    beam_cutoff = 1.0*RPARCM; /* radians */
    break;
  case 3: //test
    beam_fwhm = 5*RPARCM;   /* radians CBI2 */
    beam_freq = 1000;          /* GHz */
    beam_cutoff = 10*RPARCM; /* radians */
    break;
  case 4:
    beam_fwhm = (9.0/60)*RPARCM*12/22;   /* radians ATCA */
    beam_freq = 691.4;          /* GHz */
    beam_cutoff = 1.0*RPARCM; /* radians */
    break;
  case 5:
    beam_fwhm = (9.0/60)*RPARCM*12/25;   /* radians VLA */
    beam_freq = 691.4;          /* GHz */
    beam_cutoff = 1.0*RPARCM; /* radians */
    break;
  case 6:
    beam_fwhm = 10.5*RPARCM;   /* radians SZA */
    beam_freq = 30.9380;          /* GHz */
    beam_cutoff = 20.0*RPARCM; /* radians */
    break;

  default:
    printf("Telescope type not defined\n");
    goToError();
    break;
  }
}

__host__ freqData getFreqs(char * file)
{
   freqData freqsAndVisibilities;
   string dir = file;
   string query;
   casa::Vector<double> pointing;
   casa::Table main_tab(dir);
   casa::Table field_tab(main_tab.keywordSet().asTable("FIELD"));
   casa::Table spectral_window_tab(main_tab.keywordSet().asTable("SPECTRAL_WINDOW"));
   casa::Table polarization_tab(main_tab.keywordSet().asTable("POLARIZATION"));
   nfields = field_tab.nrow();
   casa::ROTableRow field_row(field_tab, casa::stringToVector("REFERENCE_DIR,NAME"));
   fields = (Field*)malloc(nfields*sizeof(Field));
   for(int f=0; f<nfields; f++){
     const casa::TableRecord &values = field_row.get(f);
     pointing = values.asArrayDouble("REFERENCE_DIR");
     fields[f].obsra = pointing[0];
     fields[f].obsdec = pointing[1];
   }

   nsamples = main_tab.nrow();
   if (nsamples == 0) {
      printf("ERROR : nsamples is zero... exiting....\n");
      exit(-1);
   }

  casa::ROArrayColumn<casa::Double> chan_freq_col(spectral_window_tab,"CHAN_FREQ");  //NUMBER OF SPW
  freqsAndVisibilities.n_internal_frequencies = spectral_window_tab.nrow();

  freqsAndVisibilities.channels = (int*)malloc(freqsAndVisibilities.n_internal_frequencies*sizeof(int));
  casa::ROScalarColumn<casa::Int> n_chan_freq(spectral_window_tab,"NUM_CHAN");
  for(int i = 0; i < freqsAndVisibilities.n_internal_frequencies; i++){
    freqsAndVisibilities.channels[i] = n_chan_freq(i);
  }

  int total_frequencies = 0;
  for(int i=0; i <freqsAndVisibilities.n_internal_frequencies; i++){
    for(int j=0; j < freqsAndVisibilities.channels[i]; j++){
      total_frequencies++;
    }
  }

  freqsAndVisibilities.total_frequencies = total_frequencies;
  for(int f=0; f<nfields; f++){
    fields[f].numVisibilitiesPerFreq = (long*)malloc(freqsAndVisibilities.total_frequencies*sizeof(long));
    for(int i=0;i<freqsAndVisibilities.total_frequencies;i++){
      fields[f].numVisibilitiesPerFreq[i] = 0;
    }
  }

  casa::ROScalarColumn<casa::Int> n_corr(polarization_tab,"NUM_CORR");
  nstokes=n_corr(0);

  casa::ROTableRow row(main_tab, casa::stringToVector("FLAG,FLAG_ROW,FIELD_ID,UVW,WEIGHT,SIGMA,ANTENNA1,ANTENNA2,TIME,EXPOSURE,DATA,DATA_DESC_ID"));
  casa::Vector<casa::Bool> auxbool;
  bool flag;
  int spw, field, counter;
  for(int f=0; f<nfields; f++){
    counter = 0;
    for(int i=0; i < freqsAndVisibilities.n_internal_frequencies; i++){
      for(int j=0; j < freqsAndVisibilities.channels[i]; j++){
        for (int k=0; k < nsamples; k++){
          const casa::TableRecord &values = row.get(k);
          flag = values.asBool("FLAG_ROW");
          field = values.asInt("FIELD_ID");
          spw = values.asInt("DATA_DESC_ID");
          casa::Array<casa::Bool> flagCol = values.asArrayBool("FLAG");
          if(field == f && spw == i && flag == false){
            for (int sto=0; sto<nstokes; sto++){
              auxbool = flagCol[j][sto];
              if(auxbool[0] == false){
                fields[f].numVisibilitiesPerFreq[counter]++;
              }
            }
          }else continue;
        }
        counter++;
      }
    }
  }

  return freqsAndVisibilities;
}

__host__ long NearestPowerOf2(long x)
{
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return ++x;
}


bool isPow2(unsigned int x)
{
    return ((x&(x-1))==0);
}


__host__ void readInputDat(char *file)
{
  FILE *fp;
  char item[50];
  float status;
  if((fp = fopen(file, "r")) == NULL){
    printf("ERROR. The input file wasn't provided by the user.\n");
    goToError();
  }else{
    while(true){
      int ret = fscanf(fp, "%s %e", item, &status);

      if(ret==EOF){
        break;
      }else{
        if (strcmp(item,"lambda_entropy")==0) {
          if(lambda == -1){
            lambda = status;
          }
        }else if (strcmp(item,"noise_cut")==0){
          if(noise_cut == -1){
            noise_cut = status;
          }
        }else if (strcmp(item,"t_telescope")==0){
          t_telescope = status;
        }else if(strcmp(item,"minpix")==0){
          if(minpix == -1){
            minpix = status;
          }
        } else if(strcmp(item,"ftol")==0){
          ftol = status;
        } else if(strcmp(item,"random_probability")==0){
          if(random_probability == -1){
            random_probability = status;
          }
        }else{
          printf("Keyword not defined in input\n");
          goToError();
        }
      }
    }
  }
}
__host__ void readMS(char *file, char *file2, Field *fields) {
  ///////////////////////////////////////////////////FITS READING///////////////////////////////////////////////////////////
  status_mod_in = 0;
  int status_noise = 0;
  fits_open_file(&mod_in, file2, 0, &status_mod_in);
  if (status_mod_in) {
    fits_report_error(stderr, status_mod_in); /* print error message */
    goToError();
  }


  fits_read_key(mod_in, TFLOAT, "CDELT1", &DELTAX, NULL, &status_mod_in);
  fits_read_key(mod_in, TFLOAT, "CDELT2", &DELTAY, NULL, &status_mod_in);
  fits_read_key(mod_in, TDOUBLE, "CRVAL1", &ra, NULL, &status_mod_in);
  fits_read_key(mod_in, TDOUBLE, "CRVAL2", &dec, NULL, &status_mod_in);
  fits_read_key(mod_in, TINT, "CRPIX1", &crpix1, NULL, &status_mod_in);
  fits_read_key(mod_in, TINT, "CRPIX2", &crpix2, NULL, &status_mod_in);
  fits_read_key(mod_in, TLONG, "NAXIS1", &M, NULL, &status_mod_in);
  fits_read_key(mod_in, TLONG, "NAXIS2", &N, NULL, &status_mod_in);
  fits_read_key(mod_in, TFLOAT, "BMAJ", &beam_bmaj, NULL, &status_mod_in);
  fits_read_key(mod_in, TFLOAT, "BMIN", &beam_bmin, NULL, &status_mod_in);
  fits_read_key(mod_in, TFLOAT, "NOISE", &beam_noise, NULL, &status_noise);
  if (status_mod_in) {
    fits_report_error(stderr, status_mod_in); /* print error message */
    goToError();
  }

  if(status_noise){
    beam_noise = b_noise_aux;
  }

  beam_bmaj = beam_bmaj/-DELTAX;
  beam_bmin = beam_bmin/-DELTAX;

  if(verbose_flag){
    printf("FITS Files READ\n");
  }

  char *error = 0;
  int g = 0, h = 0;
  string query;
  string dir = file;
  casa::Table main_tab(dir);
  casa::Table field_tab(main_tab.keywordSet().asTable("FIELD"));
  casa::Table spectral_window_tab(main_tab.keywordSet().asTable("SPECTRAL_WINDOW"));
  casa::Table polarization_tab(main_tab.keywordSet().asTable("POLARIZATION"));

  casa::ROArrayColumn<casa::Int> correlation_col(polarization_tab,"CORR_TYPE");
  casa::Vector<int> polarizations;
  polarizations=correlation_col(0);

  casa::ROArrayColumn<casa::Double> chan_freq_col(spectral_window_tab,"CHAN_FREQ");

  casa::ROTableRow row(main_tab, casa::stringToVector("FLAG,FLAG_ROW,FIELD_ID,UVW,WEIGHT,SIGMA,ANTENNA1,ANTENNA2,TIME,EXPOSURE,DATA,DATA_DESC_ID"));
  casa::Vector<casa::Bool> auxbool;
  casa::Vector<float> v;
  casa::Vector<float> weights;
  casa::Vector<double> uvw;
  bool flag;
  int spw, field;

  if(random_probability != 1.0){
    float u;
    SelectStream(0);
    PutSeed(1);
    for(int f=0; f<nfields; f++){
      g=0;
      for(int i=0; i < data.n_internal_frequencies; i++){
        for(int j=0; j < data.channels[i]; j++){
          for (int k=0; k < nsamples; k++){
            const casa::TableRecord &values = row.get(k);
            uvw = values.asArrayDouble("UVW");
            flag = values.asBool("FLAG_ROW");
            spw = values.asInt("DATA_DESC_ID");
            field = values.asInt("FIELD_ID");
            casa::Array<casa::Complex> dataCol = values.asArrayComplex ("DATA");
            casa::Array<casa::Bool> flagCol = values.asArrayBool("FLAG");
            weights=values.asArrayFloat ("WEIGHT");
            if(field == f && spw == i && flag == false){
              for (int sto=0; sto<nstokes; sto++){
                auxbool = flagCol[j][sto];
                if(auxbool[0] == false){
                  u = Random();
                  if(u<random_probability){
                    fields[f].visibilities[g].stokes[h] = polarizations[sto];
                    fields[f].visibilities[g].u[h] = uvw[0];
                    fields[f].visibilities[g].v[h] = uvw[1];
                    v = casa::real(dataCol[j][sto]);
                    fields[f].visibilities[g].Vo[h].x = v[0];
                    v = casa::imag(dataCol[j][sto]);
                    fields[f].visibilities[g].Vo[h].y = v[0];
                    fields[f].visibilities[g].weight[h] = weights[sto];
                    h++;
                  }else{
                    fields[f].visibilities[g].stokes[h] = polarizations[sto];
                    fields[f].visibilities[g].u[h] = uvw[0];
                    fields[f].visibilities[g].v[h] = uvw[1];
                    v = casa::real(dataCol[j][sto]);
                    fields[f].visibilities[g].Vo[h].x = v[0];
                    //fields[f].visibilities[g].Vo[h].x = 0.0;
                    v = casa::imag(dataCol[j][sto]);
                    fields[f].visibilities[g].Vo[h].y = v[0];
                    //fields[f].visibilities[g].Vo[h].y = 0.0;
                    fields[f].visibilities[g].weight[h] = 0.0;
                    h++;
                  }
                }
              }
            }else continue;
          }
          /*fields[f].numVisibilitiesPerFreq[g] = h;
          realloc(fields[f].visibilities[g].stokes, h*sizeof(int));
          realloc(fields[f].visibilities[g].u, h*sizeof(float));
          realloc(fields[f].visibilities[g].v, h*sizeof(float));
          realloc(fields[f].visibilities[g].Vo, h*sizeof(cufftComplex));
          realloc(fields[f].visibilities[g].weight, h*sizeof(float));*/
          h=0;
          g++;
        }
      }
    }
  }else{
    for(int f=0; f<nfields; f++){
      g=0;
      for(int i=0; i < data.n_internal_frequencies; i++){
        for(int j=0; j < data.channels[i]; j++){
          for (int k=0; k < nsamples; k++){
            const casa::TableRecord &values = row.get(k);
            uvw = values.asArrayDouble("UVW");
            flag = values.asBool("FLAG_ROW");
            spw = values.asInt("DATA_DESC_ID");
            field = values.asInt("FIELD_ID");
            casa::Array<casa::Complex> dataCol = values.asArrayComplex("DATA");
            casa::Array<casa::Bool> flagCol = values.asArrayBool("FLAG");
            weights=values.asArrayFloat("WEIGHT");
            if(field == f && spw == i && flag == false){
              for (int sto=0; sto<nstokes; sto++) {
                auxbool = flagCol[j][sto];
                if(auxbool[0] == false){
                  fields[f].visibilities[g].stokes[h] = polarizations[sto];
                  fields[f].visibilities[g].u[h] = uvw[0];
                  fields[f].visibilities[g].v[h] = uvw[1];
                  v = casa::real(dataCol[j][sto]);
                  fields[f].visibilities[g].Vo[h].x = v[0];
                  v = casa::imag(dataCol[j][sto]);
                  fields[f].visibilities[g].Vo[h].y = v[0];
                  fields[f].visibilities[g].weight[h] = weights[sto];
                  h++;
                }
              }
            }else continue;
          }
          h=0;
          g++;
        }
      }
    }
  }

  for(int f=0;f<nfields;f++){
    h = 0;
    for(int i = 0; i < data.n_internal_frequencies; i++){
      casa::Vector<double> chan_freq_vector;
      chan_freq_vector=chan_freq_col(i);
      for(int j = 0; j < data.channels[i]; j++){
        fields[f].visibilities[h].freq = chan_freq_vector[j];
        h++;
      }
    }
  }

  for(int f=0;f<nfields;f++){
    h = 0;
    fields[f].valid_frequencies = 0;
    for(int i = 0; i < data.n_internal_frequencies; i++){
      for(int j = 0; j < data.channels[i]; j++){
        if(fields[f].numVisibilitiesPerFreq[i] != 0){
          fields[f].valid_frequencies++;
        }
        h++;
      }
    }
  }


}

__host__ void MScopy(char const *in_dir, char const *in_dir_dest) {
  string dir_origin=in_dir;
  string dir_dest=in_dir_dest;

  casa::Table tab_src(dir_origin);
  tab_src.deepCopy(dir_dest,casa::Table::New);
  if (verbose_flag) {
      printf("Copied\n");
  }


}



__host__ void residualsToHost(Field *fields, freqData data){
  printf("Saving residuals to host memory\n");
  if(num_gpus == 1){
    for(int f=0; f<nfields;f++){
      for(int i=0; i<data.total_frequencies; i++){
        gpuErrchk(cudaMemcpy(fields[f].visibilities[i].Vm, fields[f].device_visibilities[i].Vm, sizeof(cufftComplex)*fields[f].numVisibilitiesPerFreq[i], cudaMemcpyDeviceToHost));
      }
    }
  }else{
    for(int f=0; f<nfields;f++){
      for(int i=0; i<data.total_frequencies; i++){
        cudaSetDevice((i%num_gpus) + firstgpu);
        gpuErrchk(cudaMemcpy(fields[f].visibilities[i].Vm, fields[f].device_visibilities[i].Vm, sizeof(cufftComplex)*fields[f].numVisibilitiesPerFreq[i], cudaMemcpyDeviceToHost));
      }
    }
  }

  for(int f=0; f<nfields;f++){
    for(int i=0; i<data.total_frequencies; i++){
      for(int j=0; j<fields[f].numVisibilitiesPerFreq[i];j++){
        if(fields[f].visibilities[i].u[j]<0){
          fields[f].visibilities[i].Vm[j].y *= -1;
        }
      }
    }
  }

}

__host__ void writeMS(char *infile, char *outfile, Field *fields) {
  MScopy(infile, outfile);
  char* out_col = "DATA";
  string dir=outfile;
  string query;
  casa::Table main_tab(dir,casa::Table::Update);
  string column_name=out_col;

  if (main_tab.tableDesc().isColumn(column_name))
  {
    printf("Column %s already exists... skipping creation...\n", out_col);
  }else{
    printf("Adding %s to the main table...\n", out_col);
    main_tab.addColumn(casa::ArrayColumnDesc <casa::Complex>(column_name,"created by gpuvmem"));
    main_tab.flush();
  }

  if (column_name!="DATA")
  {
     query="UPDATE "+dir+" set "+column_name+"=DATA";
     printf("Duplicating DATA column into %s\n", out_col);
     casa::tableCommand(query);
  }

  casa::TableRow row(main_tab, casa::stringToVector(column_name+",FLAG,FIELD_ID,WEIGHT,FLAG_ROW,DATA_DESC_ID"));
  casa::Complex comp;
  casa::Vector<casa::Bool> auxbool;
  casa::Vector<float> weights;
  bool flag;
  int spw, field, h = 0, g = 0;
  for(int f=0; f<nfields; f++){
    g=0;
    for(int i=0; i < data.n_internal_frequencies; i++){
      for(int j=0; j < data.channels[i]; j++){
        for (int k=0; k < nsamples; k++){
          const casa::TableRecord &values = row.get(k);
          flag = values.asBool("FLAG_ROW");
          spw = values.asInt("DATA_DESC_ID");
          field = values.asInt("FIELD_ID");
          casa::Array<casa::Bool> flagCol = values.asArrayBool("FLAG");
          casa::Array<casa::Complex> dataCol = values.asArrayComplex(column_name);
          weights=values.asArrayFloat("WEIGHT");
          if(field == f && spw == i && flag == false){
            for (int sto=0; sto<nstokes; sto++){
              auxbool = flagCol[j][sto];
              if(auxbool[0] == false){
                comp.real() = fields[f].visibilities[g].Vo[h].x - fields[f].visibilities[g].Vm[h].x;
                comp.imag() = fields[f].visibilities[g].Vo[h].y - fields[f].visibilities[g].Vm[h].y;
                dataCol[j][sto] = comp;
                if(random_probability != 1.0){
                  weights[sto] = fields[f].visibilities[g].weight[h];
                }
                h++;
              }
            }
            row.put(k);
          }else continue;
        }
        h=0;
        g++;
      }
    }
  }
  main_tab.flush();

}


__host__ void print_warranty() {
  printf("THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY \
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT \
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM 'AS IS' WITHOUT WARRANTY \
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, \
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR \
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM \
IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF \
ALL NECESSARY SERVICING, REPAIR OR CORRECTION.\n");
}

__host__ void print_copyright() {
  printf("   TERMS AND CONDITIONS \n"
  " \n"
  "0. Definitions. \n"
  " \n"
  "'This License' refers to version 3 of the GNU General Public License. \n"
  " \n"
  "'Copyright' also means copyright-like laws that apply to other kinds of \n"
  "works, such as semiconductor masks. \n"
  " \n"
  "'The Program' refers to any copyrightable work licensed under this \n"
  "License.  Each licensee is addressed as 'you'.  'Licensees' and \n"
  "'recipients' may be individuals or organizations. \n"
  " \n"
  "To 'modify' a work means to copy from or adapt all or part of the work \n"
  "in a fashion requiring copyright permission, other than the making of an \n"
  "exact copy.  The resulting work is called a 'modified version' of the \n"
  "earlier work or a work 'based on' the earlier work. \n"
  " \n"
  "A 'covered work' means either the unmodified Program or a work based \n"
  "on the Program. \n"
  " \n"
  "To 'propagate' a work means to do anything with it that, without \n"
  "permission, would make you directly or secondarily liable for \n"
  "infringement under applicable copyright law, except executing it on a \n"
  "computer or modifying a private copy.  Propagation includes copying, \n"
  "distribution (with or without modification), making available to the \n"
  "public, and in some countries other activities as well. \n"
  " \n"
  "To 'convey' a work means any kind of propagation that enables other \n"
  "parties to make or receive copies.  Mere interaction with a user through \n"
  "a computer network, with no transfer of a copy, is not conveying. \n"
  " \n"
  "An interactive user interface displays 'Appropriate Legal Notices' \n"
  "to the extent that it includes a convenient and prominently visible \n"
  "feature that (1) displays an appropriate copyright notice, and (2) \n"
  "tells the user that there is no warranty for the work (except to the \n"
  "extent that warranties are provided), that licensees may convey the \n"
  "work under this License, and how to view a copy of this License.  If \n"
  "the interface presents a list of user commands or options, such as a \n"
  "menu, a prominent item in the list meets this criterion. \n"
  " \n"
  "1. Source Code. \n"
  " \n"
  "The 'source code' for a work means the preferred form of the work \n"
  "for making modifications to it.  'Object code' means any non-source \n"
  "form of a work. \n"
  " \n"
  "A 'Standard Interface' means an interface that either is an official \n"
  "standard defined by a recognized standards body, or, in the case of \n"
  "interfaces specified for a particular programming language, one that \n"
  "is widely used among developers working in that language. \n"
  " \n"
  "The 'System Libraries' of an executable work include anything, other \n"
  "than the work as a whole, that (a) is included in the normal form of \n"
  "packaging a Major Component, but which is not part of that Major \n"
  "Component, and (b) serves only to enable use of the work with that \n"
  "Major Component, or to implement a Standard Interface for which an \n"
  "implementation is available to the public in source code form.  A \n"
  "'Major Component', in this context, means a major essential component \n"
  "(kernel, window system, and so on) of the specific operating system \n"
  "(if any) on which the executable work runs, or a compiler used to \n"
  "produce the work, or an object code interpreter used to run it. \n"
  " \n"
  "The 'Corresponding Source' for a work in object code form means all \n"
  "the source code needed to generate, install, and (for an executable \n"
  "work) run the object code and to modify the work, including scripts to \n"
  "control those activities.  However, it does not include the work's \n"
  "System Libraries, or general-purpose tools or generally available free \n"
  "programs which are used unmodified in performing those activities but \n"
  "which are not part of the work.  For example, Corresponding Source \n"
  "includes interface definition files associated with source files for \n"
  "the work, and the source code for shared libraries and dynamically \n"
  "linked subprograms that the work is specifically designed to require, \n"
  "such as by intimate data communication or control flow between those \n"
  "subprograms and other parts of the work. \n"
  " \n"
  "The Corresponding Source need not include anything that users \n"
  "can regenerate automatically from other parts of the Corresponding \n"
  "Source. \n"
  " \n"
  "The Corresponding Source for a work in source code form is that \n"
  "same work. \n"
  " \n"
  "2. Basic Permissions. \n"
  " \n"
  "All rights granted under this License are granted for the term of \n"
  "copyright on the Program, and are irrevocable provided the stated \n"
  "conditions are met.  This License explicitly affirms your unlimited \n"
  "permission to run the unmodified Program.  The output from running a \n"
  "covered work is covered by this License only if the output, given its \n"
  "content, constitutes a covered work.  This License acknowledges your \n"
  "rights of fair use or other equivalent, as provided by copyright law. \n"
  " \n"
  "You may make, run and propagate covered works that you do not \n"
  "convey, without conditions so long as your license otherwise remains \n"
  "in force.  You may convey covered works to others for the sole purpose \n"
  "of having them make modifications exclusively for you, or provide you \n"
  "with facilities for running those works, provided that you comply with \n"
  "the terms of this License in conveying all material for which you do \n"
  "not control copyright.  Those thus making or running the covered works \n"
  "for you must do so exclusively on your behalf, under your direction \n"
  "and control, on terms that prohibit them from making any copies of \n"
  "your copyrighted material outside their relationship with you. \n"
  " \n"
  "Conveying under any other circumstances is permitted solely under \n"
  "the conditions stated below.  Sublicensing is not allowed; section 10 \n"
  "makes it unnecessary. \n"
  " \n"
  "3. Protecting Users' Legal Rights From Anti-Circumvention Law. \n"
  " \n"
  "No covered work shall be deemed part of an effective technological \n"
  "measure under any applicable law fulfilling obligations under article \n"
  "11 of the WIPO copyright treaty adopted on 20 December 1996, or \n"
  "similar laws prohibiting or restricting circumvention of such \n"
  "measures. \n"
  " \n"
  "When you convey a covered work, you waive any legal power to forbid \n"
  "circumvention of technological measures to the extent such circumvention \n"
  "is effected by exercising rights under this License with respect to \n"
  "the covered work, and you disclaim any intention to limit operation or \n"
  "modification of the work as a means of enforcing, against the work's \n"
  "users, your or third parties' legal rights to forbid circumvention of \n"
  "technological measures. \n"
  " \n"
  "4. Conveying Verbatim Copies. \n"
  " \n"
  "You may convey verbatim copies of the Program's source code as you \n"
  "receive it, in any medium, provided that you conspicuously and \n"
  "appropriately publish on each copy an appropriate copyright notice; \n"
  "keep intact all notices stating that this License and any \n"
  "non-permissive terms added in accord with section 7 apply to the code; \n"
  "keep intact all notices of the absence of any warranty; and give all \n"
  "recipients a copy of this License along with the Program. \n"
  " \n"
  "You may charge any price or no price for each copy that you convey, \n"
  "and you may offer support or warranty protection for a fee. \n"
  " \n"
  "5. Conveying Modified Source Versions. \n"
  " \n"
  "You may convey a work based on the Program, or the modifications to \n"
  "produce it from the Program, in the form of source code under the \n"
  "terms of section 4, provided that you also meet all of these conditions: \n"
  " \n"
  "a) The work must carry prominent notices stating that you modified \n"
  "it, and giving a relevant date. \n"
  " \n"
  "b) The work must carry prominent notices stating that it is \n"
  "released under this License and any conditions added under section \n"
  "7.  This requirement modifies the requirement in section 4 to \n"
  "'keep intact all notices'. \n"
  " \n"
  "c) You must license the entire work, as a whole, under this \n"
  "License to anyone who comes into possession of a copy.  This \n"
  "License will therefore apply, along with any applicable section 7 \n"
  "additional terms, to the whole of the work, and all its parts, \n"
  "regardless of how they are packaged.  This License gives no \n"
  "permission to license the work in any other way, but it does not \n"
  "invalidate such permission if you have separately received it. \n"
  " \n"
  "d) If the work has interactive user interfaces, each must display \n"
  "Appropriate Legal Notices; however, if the Program has interactive \n"
  "interfaces that do not display Appropriate Legal Notices, your \n"
  "work need not make them do so. \n"
  " \n"
  "A compilation of a covered work with other separate and independent \n"
  "works, which are not by their nature extensions of the covered work, \n"
  "and which are not combined with it such as to form a larger program, \n"
  "in or on a volume of a storage or distribution medium, is called an \n"
  "'aggregate' if the compilation and its resulting copyright are not \n"
  "used to limit the access or legal rights of the compilation's users \n"
  "beyond what the individual works permit.  Inclusion of a covered work \n"
  "in an aggregate does not cause this License to apply to the other \n"
  "parts of the aggregate. \n"
  " \n"
  "6. Conveying Non-Source Forms. \n"
  " \n"
  "You may convey a covered work in object code form under the terms \n"
  "of sections 4 and 5, provided that you also convey the \n"
  "machine-readable Corresponding Source under the terms of this License, \n"
  "in one of these ways: \n"
  " \n"
  "a) Convey the object code in, or embodied in, a physical product \n"
  "(including a physical distribution medium), accompanied by the \n"
  "Corresponding Source fixed on a durable physical medium \n"
  "customarily used for software interchange. \n"
  " \n"
  "b) Convey the object code in, or embodied in, a physical product \n"
  "(including a physical distribution medium), accompanied by a \n"
  "written offer, valid for at least three years and valid for as \n"
  "long as you offer spare parts or customer support for that product \n"
  "model, to give anyone who possesses the object code either (1) a \n"
  "copy of the Corresponding Source for all the software in the \n"
  "product that is covered by this License, on a durable physical \n"
  "medium customarily used for software interchange, for a price no \n"
  "more than your reasonable cost of physically performing this \n"
  "conveying of source, or (2) access to copy the \n"
  "Corresponding Source from a network server at no charge. \n"
  " \n"
  "c) Convey individual copies of the object code with a copy of the \n"
  "written offer to provide the Corresponding Source.  This \n"
  "alternative is allowed only occasionally and noncommercially, and \n"
  "only if you received the object code with such an offer, in accord \n"
  "with subsection 6b. \n"
  " \n"
  "d) Convey the object code by offering access from a designated \n"
  "place (gratis or for a charge), and offer equivalent access to the \n"
  "Corresponding Source in the same way through the same place at no \n"
  "further charge.  You need not require recipients to copy the \n"
  "Corresponding Source along with the object code.  If the place to \n"
  "copy the object code is a network server, the Corresponding Source \n"
  "may be on a different server (operated by you or a third party) \n"
  "that supports equivalent copying facilities, provided you maintain \n"
  "clear directions next to the object code saying where to find the \n"
  "Corresponding Source.  Regardless of what server hosts the \n"
  "Corresponding Source, you remain obligated to ensure that it is \n"
  "available for as long as needed to satisfy these requirements. \n"
  " \n"
  "e) Convey the object code using peer-to-peer transmission, provided \n"
  "you inform other peers where the object code and Corresponding \n"
  "Source of the work are being offered to the general public at no \n"
  "charge under subsection 6d. \n"
  " \n"
  "A separable portion of the object code, whose source code is excluded \n"
  "from the Corresponding Source as a System Library, need not be \n"
  "included in conveying the object code work. \n"
  " \n"
  "A 'User Product' is either (1) a 'consumer product', which means any \n"
  "tangible personal property which is normally used for personal, family, \n"
  "or household purposes, or (2) anything designed or sold for incorporation \n"
  "into a dwelling.  In determining whether a product is a consumer product, \n"
  "doubtful cases shall be resolved in favor of coverage.  For a particular \n"
  "product received by a particular user, 'normally used' refers to a \n"
  "typical or common use of that class of product, regardless of the status \n"
  "of the particular user or of the way in which the particular user \n"
  "actually uses, or expects or is expected to use, the product.  A product \n"
  "is a consumer product regardless of whether the product has substantial \n"
  "commercial, industrial or non-consumer uses, unless such uses represent \n"
  "the only significant mode of use of the product. \n"
  " \n"
  "'Installation Information' for a User Product means any methods, \n"
  "procedures, authorization keys, or other information required to install \n"
  "and execute modified versions of a covered work in that User Product from \n"
  "a modified version of its Corresponding Source.  The information must \n"
  "suffice to ensure that the continued functioning of the modified object \n"
  "code is in no case prevented or interfered with solely because \n"
  "modification has been made. \n"
  " \n"
  "If you convey an object code work under this section in, or with, or \n"
  "specifically for use in, a User Product, and the conveying occurs as \n"
  "part of a transaction in which the right of possession and use of the \n"
  "User Product is transferred to the recipient in perpetuity or for a \n"
  "fixed term (regardless of how the transaction is characterized), the \n"
  "Corresponding Source conveyed under this section must be accompanied \n"
  "by the Installation Information.  But this requirement does not apply \n"
  "if neither you nor any third party retains the ability to install \n"
  "modified object code on the User Product (for example, the work has \n"
  "been installed in ROM). \n"
  " \n"
  "The requirement to provide Installation Information does not include a \n"
  "requirement to continue to provide support service, warranty, or updates \n"
  "for a work that has been modified or installed by the recipient, or for \n"
  "the User Product in which it has been modified or installed.  Access to a \n"
  "network may be denied when the modification itself materially and \n"
  "adversely affects the operation of the network or violates the rules and \n"
  "protocols for communication across the network. \n"
  " \n"
  "Corresponding Source conveyed, and Installation Information provided, \n"
  "in accord with this section must be in a format that is publicly \n"
  "documented (and with an implementation available to the public in \n"
  "source code form), and must require no special password or key for \n"
  "unpacking, reading or copying. \n"
  " \n"
  "7. Additional Terms. \n"
  " \n"
  "'Additional permissions' are terms that supplement the terms of this \n"
  "License by making exceptions from one or more of its conditions. \n"
  "Additional permissions that are applicable to the entire Program shall \n"
  "be treated as though they were included in this License, to the extent \n"
  "that they are valid under applicable law.  If additional permissions \n"
  "apply only to part of the Program, that part may be used separately \n"
  "under those permissions, but the entire Program remains governed by \n"
  "this License without regard to the additional permissions. \n"
  " \n"
  "When you convey a copy of a covered work, you may at your option \n"
  "remove any additional permissions from that copy, or from any part of \n"
  "it.  (Additional permissions may be written to require their own \n"
  "removal in certain cases when you modify the work.)  You may place \n"
  "additional permissions on material, added by you to a covered work, \n"
  "for which you have or can give appropriate copyright permission. \n"
  " \n"
  "Notwithstanding any other provision of this License, for material you \n"
  "add to a covered work, you may (if authorized by the copyright holders of \n"
  "that material) supplement the terms of this License with terms: \n"
  " \n"
  "a) Disclaiming warranty or limiting liability differently from the \n"
  "terms of sections 15 and 16 of this License; or \n"
  " \n"
  "b) Requiring preservation of specified reasonable legal notices or \n"
  "author attributions in that material or in the Appropriate Legal \n"
  "Notices displayed by works containing it; or \n"
  " \n"
  "c) Prohibiting misrepresentation of the origin of that material, or \n"
  "requiring that modified versions of such material be marked in \n"
  "reasonable ways as different from the original version; or \n"
  " \n"
  "d) Limiting the use for publicity purposes of names of licensors or \n"
  "authors of the material; or \n"
  " \n"
  "e) Declining to grant rights under trademark law for use of some \n"
  "trade names, trademarks, or service marks; or \n"
  " \n"
  "f) Requiring indemnification of licensors and authors of that \n"
  "material by anyone who conveys the material (or modified versions of \n"
  "it) with contractual assumptions of liability to the recipient, for \n"
  "any liability that these contractual assumptions directly impose on \n"
  "those licensors and authors. \n"
  " \n"
  "All other non-permissive additional terms are considered 'further \n"
  "restrictions' within the meaning of section 10.  If the Program as you \n"
  "received it, or any part of it, contains a notice stating that it is \n"
  "governed by this License along with a term that is a further \n"
  "restriction, you may remove that term.  If a license document contains \n"
  "a further restriction but permits relicensing or conveying under this \n"
  "License, you may add to a covered work material governed by the terms \n"
  "of that license document, provided that the further restriction does \n"
  "not survive such relicensing or conveying. \n"
  " \n"
  "If you add terms to a covered work in accord with this section, you \n"
  "must place, in the relevant source files, a statement of the \n"
  "additional terms that apply to those files, or a notice indicating \n"
  "where to find the applicable terms. \n"
  " \n"
  "Additional terms, permissive or non-permissive, may be stated in the \n"
  "form of a separately written license, or stated as exceptions; \n"
  "the above requirements apply either way. \n"
  " \n"
  "8. Termination. \n"
  " \n"
  "You may not propagate or modify a covered work except as expressly \n"
  "provided under this License.  Any attempt otherwise to propagate or \n"
  "modify it is void, and will automatically terminate your rights under \n"
  "this License (including any patent licenses granted under the third \n"
  "paragraph of section 11). \n"
  " \n"
  "However, if you cease all violation of this License, then your \n"
  "license from a particular copyright holder is reinstated (a) \n"
  "provisionally, unless and until the copyright holder explicitly and \n"
  "finally terminates your license, and (b) permanently, if the copyright \n"
  "holder fails to notify you of the violation by some reasonable means \n"
  "prior to 60 days after the cessation. \n"
  " \n"
  "Moreover, your license from a particular copyright holder is \n"
  "reinstated permanently if the copyright holder notifies you of the \n"
  "violation by some reasonable means, this is the first time you have \n"
  "received notice of violation of this License (for any work) from that \n"
  "copyright holder, and you cure the violation prior to 30 days after \n"
  "your receipt of the notice. \n"
  " \n"
  "Termination of your rights under this section does not terminate the \n"
  "licenses of parties who have received copies or rights from you under \n"
  "this License.  If your rights have been terminated and not permanently \n"
  "reinstated, you do not qualify to receive new licenses for the same \n"
  "material under section 10. \n"
  " \n"
  "9. Acceptance Not Required for Having Copies. \n"
  " \n"
  "You are not required to accept this License in order to receive or \n"
  "run a copy of the Program.  Ancillary propagation of a covered work \n"
  "occurring solely as a consequence of using peer-to-peer transmission \n"
  "to receive a copy likewise does not require acceptance.  However, \n"
  "nothing other than this License grants you permission to propagate or \n"
  "modify any covered work.  These actions infringe copyright if you do \n"
  "not accept this License.  Therefore, by modifying or propagating a \n"
  "covered work, you indicate your acceptance of this License to do so. \n"
  " \n"
  "10. Automatic Licensing of Downstream Recipients. \n"
  " \n"
  "Each time you convey a covered work, the recipient automatically \n"
  "receives a license from the original licensors, to run, modify and \n"
  "propagate that work, subject to this License.  You are not responsible \n"
  "for enforcing compliance by third parties with this License. \n"
  " \n"
  "An 'entity transaction' is a transaction transferring control of an \n"
  "organization, or substantially all assets of one, or subdividing an \n"
  "organization, or merging organizations.  If propagation of a covered \n"
  "work results from an entity transaction, each party to that \n"
  "transaction who receives a copy of the work also receives whatever \n"
  "licenses to the work the party's predecessor in interest had or could \n"
  "give under the previous paragraph, plus a right to possession of the \n"
  "Corresponding Source of the work from the predecessor in interest, if \n"
  "the predecessor has it or can get it with reasonable efforts. \n"
  " \n"
  "You may not impose any further restrictions on the exercise of the \n"
  "rights granted or affirmed under this License.  For example, you may \n"
  "not impose a license fee, royalty, or other charge for exercise of \n"
  "rights granted under this License, and you may not initiate litigation \n"
  "(including a cross-claim or counterclaim in a lawsuit) alleging that \n"
  "any patent claim is infringed by making, using, selling, offering for \n"
  "sale, or importing the Program or any portion of it. \n"
  " \n"
  "11. Patents. \n"
  " \n"
  "A 'contributor' is a copyright holder who authorizes use under this \n"
  "License of the Program or a work on which the Program is based.  The \n"
  "work thus licensed is called the contributor's 'contributor version'. \n"
  " \n"
  "A contributor's 'essential patent claims' are all patent claims \n"
  "owned or controlled by the contributor, whether already acquired or \n"
  "hereafter acquired, that would be infringed by some manner, permitted \n"
  "by this License, of making, using, or selling its contributor version, \n"
  "but do not include claims that would be infringed only as a \n"
  "consequence of further modification of the contributor version.  For \n"
  "purposes of this definition, 'control' includes the right to grant \n"
  "patent sublicenses in a manner consistent with the requirements of \n"
  "this License. \n"
  " \n"
  "Each contributor grants you a non-exclusive, worldwide, royalty-free \n"
  "patent license under the contributor's essential patent claims, to \n"
  "make, use, sell, offer for sale, import and otherwise run, modify and \n"
  "propagate the contents of its contributor version. \n"
  " \n"
  "In the following three paragraphs, a 'patent license' is any express \n"
  "agreement or commitment, however denominated, not to enforce a patent \n"
  "(such as an express permission to practice a patent or covenant not to \n"
  "sue for patent infringement).  To 'grant' such a patent license to a \n"
  "party means to make such an agreement or commitment not to enforce a \n"
  "patent against the party. \n"
  " \n"
  "If you convey a covered work, knowingly relying on a patent license, \n"
  "and the Corresponding Source of the work is not available for anyone \n"
  "to copy, free of charge and under the terms of this License, through a \n"
  "publicly available network server or other readily accessible means, \n"
  "then you must either (1) cause the Corresponding Source to be so \n"
  "available, or (2) arrange to deprive yourself of the benefit of the \n"
  "patent license for this particular work, or (3) arrange, in a manner \n"
  "consistent with the requirements of this License, to extend the patent \n"
  "license to downstream recipients.  'Knowingly relying' means you have \n"
  "actual knowledge that, but for the patent license, your conveying the \n"
  "covered work in a country, or your recipient's use of the covered work \n"
  "in a country, would infringe one or more identifiable patents in that \n"
  "country that you have reason to believe are valid. \n"
  " \n"
  "If, pursuant to or in connection with a single transaction or \n"
  "arrangement, you convey, or propagate by procuring conveyance of, a \n"
  "covered work, and grant a patent license to some of the parties \n"
  "receiving the covered work authorizing them to use, propagate, modify \n"
  "or convey a specific copy of the covered work, then the patent license \n"
  "you grant is automatically extended to all recipients of the covered \n"
  "work and works based on it. \n"
  " \n"
  "A patent license is 'discriminatory' if it does not include within \n"
  "the scope of its coverage, prohibits the exercise of, or is \n"
  "conditioned on the non-exercise of one or more of the rights that are \n"
  "specifically granted under this License.  You may not convey a covered \n"
  "work if you are a party to an arrangement with a third party that is \n"
  "in the business of distributing software, under which you make payment \n"
  "to the third party based on the extent of your activity of conveying \n"
  "the work, and under which the third party grants, to any of the \n"
  "parties who would receive the covered work from you, a discriminatory \n"
  "patent license (a) in connection with copies of the covered work \n"
  "conveyed by you (or copies made from those copies), or (b) primarily \n"
  "for and in connection with specific products or compilations that \n"
  "contain the covered work, unless you entered into that arrangement, \n"
  "or that patent license was granted, prior to 28 March 2007. \n"
  " \n"
  "Nothing in this License shall be construed as excluding or limiting \n"
  "any implied license or other defenses to infringement that may \n"
  "otherwise be available to you under applicable patent law. \n"
  " \n"
  "12. No Surrender of Others' Freedom. \n"
  " \n"
  "If conditions are imposed on you (whether by court order, agreement or \n"
  "otherwise) that contradict the conditions of this License, they do not \n"
  "excuse you from the conditions of this License.  If you cannot convey a \n"
  "covered work so as to satisfy simultaneously your obligations under this \n"
  "License and any other pertinent obligations, then as a consequence you may \n"
  "not convey it at all.  For example, if you agree to terms that obligate you \n"
  "to collect a royalty for further conveying from those to whom you convey \n"
  "the Program, the only way you could satisfy both those terms and this \n"
  "License would be to refrain entirely from conveying the Program. \n"
  " \n"
  "13. Use with the GNU Affero General Public License. \n"
  " \n"
  "Notwithstanding any other provision of this License, you have \n"
  "permission to link or combine any covered work with a work licensed \n"
  "under version 3 of the GNU Affero General Public License into a single \n"
  "combined work, and to convey the resulting work.  The terms of this \n"
  "License will continue to apply to the part which is the covered work, \n"
  "but the special requirements of the GNU Affero General Public License, \n"
  "section 13, concerning interaction through a network will apply to the \n"
  "combination as such. \n"
  " \n"
  "14. Revised Versions of this License. \n"
  " \n"
  "The Free Software Foundation may publish revised and/or new versions of \n"
  "the GNU General Public License from time to time.  Such new versions will \n"
  "be similar in spirit to the present version, but may differ in detail to \n"
  "address new problems or concerns. \n"
  " \n"
  "Each version is given a distinguishing version number.  If the \n"
  "Program specifies that a certain numbered version of the GNU General \n"
  "Public License 'or any later version' applies to it, you have the \n"
  "option of following the terms and conditions either of that numbered \n"
  "version or of any later version published by the Free Software \n"
  "Foundation.  If the Program does not specify a version number of the \n"
  "GNU General Public License, you may choose any version ever published \n"
  "by the Free Software Foundation. \n"
  " \n"
  "If the Program specifies that a proxy can decide which future \n"
  "versions of the GNU General Public License can be used, that proxy's \n"
  "public statement of acceptance of a version permanently authorizes you \n"
  "to choose that version for the Program. \n"
  " \n"
  "Later license versions may give you additional or different \n"
  "permissions.  However, no additional obligations are imposed on any \n"
  "author or copyright holder as a result of your choosing to follow a \n"
  "later version. \n"
  " \n"
  "15. Disclaimer of Warranty. \n"
  " \n"
  "THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY \n"
  "APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT \n"
  "HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM 'AS IS' WITHOUT WARRANTY \n"
  "OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, \n"
  "THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR \n"
  "PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM \n"
  "IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF \n"
  "ALL NECESSARY SERVICING, REPAIR OR CORRECTION. \n"
  " \n"
  "16. Limitation of Liability. \n"
  " \n"
  "IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING \n"
  "WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS \n"
  "THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY \n"
  "GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE \n"
  "USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF \n"
  "DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD \n"
  "PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), \n"
  "EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF \n"
  "SUCH DAMAGES. \n"
  " \n"
  "17. Interpretation of Sections 15 and 16. \n"
  " \n"
  "If the disclaimer of warranty and limitation of liability provided \n"
  "above cannot be given local legal effect according to their terms, \n"
  "reviewing courts shall apply local law that most closely approximates \n"
  "an absolute waiver of all civil liability in connection with the \n"
  "Program, unless a warranty or assumption of liability accompanies a \n"
  "copy of the Program in return for a fee. \n"
  " \n"
  " END OF TERMS AND CONDITIONS \n\n");
}

__host__ void print_help() {
	printf("Example: ./bin/gpuvmem options [ arguments ...]\n");
	printf("    -h  --help             Shows this\n");
  printf(	"   -X  --blockSizeX       Block X Size for Image (Needs to be pow of 2)\n");
  printf(	"   -Y  --blockSizeY       Block Y Size for Image (Needs to be pow of 2)\n");
  printf(	"   -V  --blockSizeV       Block Size for Visibilities (Needs to be pow of 2)\n");
  printf(	"   -i  --input            The name of the input file of visibilities(MS)\n");
  printf(	"   -o  --output           The name of the output file of residual visibilities(MS)\n");
  printf(	"   -O  --output-image     The name of the output image FITS file\n");
  printf("    -I  --inputdat         The name of the input file of parameters\n");
  printf("    -m  --modin            mod_in_0 FITS file\n");
  printf("    -x  --minpix           Minimum positive value of a pixel (Optional)\n");
  printf("    -n  --noise            Noise Parameter (Optional)\n");
  printf("    -N  --noise-cut        Noise-cut Parameter (Optional)\n");
  printf("    -l  --lambda           Lambda Regularization Parameter (Optional)\n");
  printf("    -r  --randoms          Percentage of data used when random sampling (Default = 1.0, optional)\n");
  printf("    -P  --prior            Prior used to regularize the solution (Default = 0 = Entropy)\n");
  printf("    -p  --path             MEM path to save FITS images. With last / included. (Example ./../mem/)\n");
  printf("    -f  --file             Output file where final objective function values are saved (Optional)\n");
  printf("    -M  --multigpu         Number of GPUs to use multiGPU image synthesis (Default OFF => 0)\n");
  printf("    -s  --select           If multigpu option is OFF, then select the GPU ID of the GPU you will work on. (Default = 0)\n");
  printf("    -t  --iterations       Number of iterations for optimization (Default = 500)\n");
  printf("    -c  --copyright        Shows copyright conditions\n");
  printf("    -w  --warranty         Shows no warranty details\n");
  printf("        --xcorr            Run gpuvmem with cross-correlation\n");
  printf("        --nopositivity     Run gpuvmem using chi2 with no posititivy restriction\n");
  printf("        --clipping         Clips the image to positive values\n");
  printf("        --verbose          Shows information through all the execution\n");
}

__host__ char *strip(const char *string, const char *chars)
{
  char * newstr = (char*)malloc(strlen(string) + 1);
  int counter = 0;

  for ( ; *string; string++) {
    if (!strchr(chars, *string)) {
      newstr[ counter ] = *string;
      ++ counter;
    }
  }

  newstr[counter] = 0;
  return newstr;
}

__host__ Vars getOptions(int argc, char **argv) {
	Vars variables;
  variables.multigpu = "NULL";
  variables.ofile = "NULL";
  variables.select = 0;
  variables.blockSizeX = -1;
  variables.blockSizeY = -1;
  variables.blockSizeV = -1;
  variables.it_max = 500;
  variables.noise = -1;
  variables.lambda = -1;
  variables.randoms = 1.0;
  variables.noise_cut = -1;
  variables.minpix = -1;
  variables.reg_term = 0;


	long next_op;
	const char* const short_op = "hcwi:o:O:I:m:x:n:N:l:r:f:M:s:p:P:X:Y:V:t:";

	const struct option long_op[] = { //Flag for help, copyright and warranty
                                    {"help", 0, NULL, 'h' },
                                    {"warranty", 0, NULL, 'w' },
                                    {"copyright", 0, NULL, 'c' },
                                    /* These options set a flag. */
                                    {"verbose", 0, &verbose_flag, 1},
                                    {"xcorr", 0, &xcorr_flag, 1},
                                    {"nopositivity", 0, &nopositivity, 1},
                                    {"clipping", 0, &clip_flag, 1},
                                    /* These options don’t set a flag. */
                                    {"input", 1, NULL, 'i' }, {"output", 1, NULL, 'o'}, {"output-image", 1, NULL, 'O'},
                                    {"inputdat", 1, NULL, 'I'}, {"modin", 1, NULL, 'm' }, {"noise", 0, NULL, 'n' },
                                    {"lambda", 0, NULL, 'l' }, {"multigpu", 0, NULL, 'M'}, {"select", 1, NULL, 's'},
                                    {"path", 1, NULL, 'p'}, {"prior", 0, NULL, 'P'}, {"blockSizeX", 1, NULL, 'X'},
                                    {"blockSizeY", 1, NULL, 'Y'}, {"blockSizeV", 1, NULL, 'V'}, {"iterations", 0, NULL, 't'},
                                    {"noise-cut", 0, NULL, 'N' }, {"minpix", 0, NULL, 'x' }, {"randoms", 0, NULL, 'r' },
                                    {"file", 0, NULL, 'f' },{ NULL, 0, NULL, 0 }};

	if (argc == 1) {
		printf(
				"ERROR. THE PROGRAM HAS BEEN EXECUTED WITHOUT THE NEEDED PARAMETERS OR OPTIONS\n");
		print_help();
		exit(EXIT_SUCCESS);
	}
  int option_index = 0;
	while (1) {
		next_op = getopt_long(argc, argv, short_op, long_op, &option_index);
		if (next_op == -1) {
			break;
		}

		switch (next_op) {
    case 0:
      /* If this option set a flag, do nothing else now. */
      if (long_op[option_index].flag != 0)
        break;
        printf ("option %s", long_op[option_index].name);
      if (optarg)
        printf (" with arg %s", optarg);
        printf ("\n");
        break;
		case 'h':
			print_help();
			exit(EXIT_SUCCESS);
    case 'w':
  		print_warranty();
  		exit(EXIT_SUCCESS);
    case 'c':
    	print_copyright();
    	exit(EXIT_SUCCESS);
		case 'i':
      variables.input = (char*) malloc((strlen(optarg)+1)*sizeof(char));
			strcpy(variables.input, optarg);
			break;
    case 'o':
      variables.output = (char*) malloc((strlen(optarg)+1)*sizeof(char));
  		strcpy(variables.output, optarg);
  		break;
    case 'O':
      variables.output_image = (char*) malloc((strlen(optarg)+1)*sizeof(char));
    	strcpy(variables.output_image, optarg);
    	break;
    case 'I':
      variables.inputdat = (char*) malloc((strlen(optarg)+1)*sizeof(char));
      strcpy(variables.inputdat, optarg);
      break;
    case 'm':
      variables.modin = (char*) malloc((strlen(optarg)+1)*sizeof(char));
    	strcpy(variables.modin, optarg);
    	break;
    case 'x':
      variables.minpix = atof(optarg);
      break;
    case 'n':
      variables.noise = atof(optarg);
      break;
    case 'N':
      variables.noise_cut = atof(optarg);
      break;
    case 'l':
      variables.lambda = atof(optarg);
      break;
    case 'p':
      variables.path = (char*) malloc((strlen(optarg)+1)*sizeof(char));
      strcpy(variables.path, optarg);
      break;
    case 'P':
      variables.reg_term = atoi(optarg);;
      break;
    case 'M':
      variables.multigpu = optarg;
      break;
    case 'r':
      variables.randoms = atof(optarg);
      break;
    case 'f':
      variables.ofile = (char*) malloc((strlen(optarg)+1)*sizeof(char));
      strcpy(variables.ofile, optarg);
      break;
    case 's':
      variables.select = atoi(optarg);
      break;
    case 'X':
      variables.blockSizeX = atoi(optarg);
      break;
    case 'Y':
      variables.blockSizeY = atoi(optarg);
      break;
    case 'V':
      variables.blockSizeV = atoi(optarg);
      break;
    case 't':
      variables.it_max = atoi(optarg);
      break;
		case '?':
			print_help();
			exit(EXIT_FAILURE);
		case -1:
			break;
		default:
      print_help();
			exit(EXIT_FAILURE);
		}
	}

  if(variables.blockSizeX == -1 && variables.blockSizeY == -1 && variables.blockSizeV == -1 ||
     strcmp(strip(variables.input, " "),"") == 0 && strcmp(strip(variables.output, " "),"") == 0 && strcmp(strip(variables.output_image, " "),"") == 0 && strcmp(strip(variables.inputdat, " "),"") == 0 ||
     strcmp(strip(variables.modin, " "),"") == 0 && strcmp(strip(variables.path, " "),"") == 0) {
        print_help();
        exit(EXIT_FAILURE);
  }

  if(!isPow2(variables.blockSizeX) && !isPow2(variables.blockSizeY) && !isPow2(variables.blockSizeV)){
    print_help();
    exit(EXIT_FAILURE);
  }

  if(strcmp(variables.multigpu,"NULL")!=0 && variables.select != 0){
    print_help();
    exit(EXIT_FAILURE);
  }
	return variables;
}

__host__ void toFitsFloat(cufftComplex *I, int iteration, long M, long N, int option)
{
	fitsfile *fpointer;
	int status = 0;
	long fpixel = 1;
	long elements = M*N;
  size_t needed;
  char *name;
	long naxes[2]={M,N};
	long naxis = 2;
  char *unit = "JY/PIXEL";
  switch(option){
    case 0:
      needed = snprintf(NULL, 0, "!%s", out_image) + 1;
      name = (char*)malloc(needed*sizeof(char));
      snprintf(name, needed*sizeof(char), "!%s", out_image);
      break;
    case 1:
      needed = snprintf(NULL, 0, "!%sMEM_%d.fits", mempath, iteration) + 1;
      name = (char*)malloc(needed*sizeof(char));
      snprintf(name, needed*sizeof(char), "!%sMEM_%d.fits", mempath, iteration);
      break;
    case 2:
      needed = snprintf(NULL, 0, "!%sMEM_V_%d.fits", mempath, iteration) + 1;
      name = (char*)malloc(needed*sizeof(char));
      snprintf(name, needed*sizeof(char), "!%sMEM_V_%d.fits", mempath, iteration);
      break;
    case 3:
      needed = snprintf(NULL, 0, "!%sMEM_VB_%d.fits", mempath, iteration) + 1;
      name = (char*)malloc(needed*sizeof(char));
      snprintf(name, needed*sizeof(char), "!%sMEM_VB_%d.fits", mempath, iteration);
      break;
    case 4:
      needed = snprintf(NULL, 0, "!%satten_%d.fits", mempath, iteration) + 1;
      name = (char*)malloc(needed*sizeof(char));
      snprintf(name, needed*sizeof(char), "!%satten_%d.fits", mempath, iteration);
      break;
    case 5:
      needed = snprintf(NULL, 0, "!%snoise_0.fits", mempath, iteration) + 1;
      name = (char*)malloc(needed*sizeof(char));
      snprintf(name, needed*sizeof(char), "!%snoise_0.fits", mempath, iteration);
      break;
    case -1:
      break;
    default:
      printf("Invalid case to FITS\n");
      goToError();
  }


	fits_create_file(&fpointer, name, &status);
  if (status) {
    fits_report_error(stderr, status); /* print error message */
    goToError();
  }
  fits_copy_header(mod_in, fpointer, &status);
  if (status) {
    fits_report_error(stderr, status); /* print error message */
    goToError();
  }
  if(option==0 || option==1){
    fits_update_key(fpointer, TSTRING, "BUNIT", unit, "Unit of measurement", &status);
  }
  cufftComplex *host_IFITS;
  host_IFITS = (cufftComplex*)malloc(M*N*sizeof(cufftComplex));
  gpuErrchk(cudaMemcpy2D(host_IFITS, sizeof(cufftComplex), I, sizeof(cufftComplex), sizeof(cufftComplex), M*N, cudaMemcpyDeviceToHost));

	float* image2D;
	image2D = (float*) malloc(M*N*sizeof(float));

  int x = M-1;
  int y = N-1;
  for(int i=0; i < M; i++){
		for(int j=0; j < N; j++){
      if(option == 0 || option == 1){
			  image2D[N*y+x] = host_IFITS[N*i+j].x * fg_scale;
      }else if (option == 2 || option == 3){
        image2D[N*i+j] = sqrt(host_IFITS[N*i+j].x * host_IFITS[N*i+j].x + host_IFITS[N*i+j].y * host_IFITS[N*i+j].y);
        //image2D[N*x+y] = host_IFITS[N*i+j].y;
      }else if(option == 4 || option == 5){
        image2D[N*i+j] = host_IFITS[N*i+j].x;
      }
      x--;
		}
    x=M-1;
    y--;
	}

	fits_write_img(fpointer, TFLOAT, fpixel, elements, image2D, &status);
  if (status) {
    fits_report_error(stderr, status); /* print error message */
    goToError();
  }
	fits_close_file(fpointer, &status);
  if (status) {
    fits_report_error(stderr, status); /* print error message */
    goToError();
  }

  free(host_IFITS);
	free(image2D);
  free(name);
}


template <bool nIsPow2>
__global__ void deviceReduceKernel(float *g_idata, float *g_odata, unsigned int n)
{
    extern __shared__ float sdata[];

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int blockSize = blockDim.x;
    unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
    unsigned int gridSize = blockSize*2*gridDim.x;

    float mySum = 0.f;

    // we reduce multiple elements per thread.  The number is determined by the
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < n)
    {
        mySum += g_idata[i];

        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < n)
            mySum += g_idata[i+blockSize];

        i += gridSize;
    }

    // each thread puts its local sum into shared memory
    sdata[tid] = mySum;
    __syncthreads();


    // do reduction in shared mem
    if ((blockSize >= 512) && (tid < 256))
    {
        sdata[tid] = mySum = mySum + sdata[tid + 256];
    }

    __syncthreads();

    if ((blockSize >= 256) &&(tid < 128))
    {
            sdata[tid] = mySum = mySum + sdata[tid + 128];
    }

     __syncthreads();

    if ((blockSize >= 128) && (tid <  64))
    {
       sdata[tid] = mySum = mySum + sdata[tid +  64];
    }

    __syncthreads();

#if (__CUDA_ARCH__ >= 300 )
    if ( tid < 32 )
    {
        // Fetch final intermediate sum from 2nd warp
        if (blockSize >=  64) mySum += sdata[tid + 32];
        // Reduce final warp using shuffle
        for (int offset = warpSize/2; offset > 0; offset /= 2)
        {
            mySum += __shfl_down(mySum, offset);
        }
    }
#else
    // fully unroll reduction within a single warp
    if ((blockSize >=  64) && (tid < 32))
    {
        sdata[tid] = mySum = mySum + sdata[tid + 32];
    }

    __syncthreads();

    if ((blockSize >=  32) && (tid < 16))
    {
        sdata[tid] = mySum = mySum + sdata[tid + 16];
    }

    __syncthreads();

    if ((blockSize >=  16) && (tid <  8))
    {
        sdata[tid] = mySum = mySum + sdata[tid +  8];
    }

    __syncthreads();

    if ((blockSize >=   8) && (tid <  4))
    {
        sdata[tid] = mySum = mySum + sdata[tid +  4];
    }

    __syncthreads();

    if ((blockSize >=   4) && (tid <  2))
    {
        sdata[tid] = mySum = mySum + sdata[tid +  2];
    }

    __syncthreads();

    if ((blockSize >=   2) && ( tid <  1))
    {
        sdata[tid] = mySum = mySum + sdata[tid +  1];
    }

    __syncthreads();
#endif

    // write result for this block to global mem
    if (tid == 0) g_odata[blockIdx.x] = mySum;
}





__host__ float deviceReduce(float *in, long N)
{
  float *device_out;
  gpuErrchk(cudaMalloc(&device_out, sizeof(float)*1024));
  gpuErrchk(cudaMemset(device_out, 0, sizeof(float)*1024));

  int threads = 512;
  int blocks = min((int(NearestPowerOf2(N)) + threads - 1) / threads, 1024);
  int smemSize = (threads <= 32) ? 2 * threads * sizeof(float) : threads * sizeof(float);

  bool isPower2 = isPow2(N);
  if(isPower2){
    deviceReduceKernel<true><<<blocks, threads, smemSize>>>(in, device_out, N);
    gpuErrchk(cudaDeviceSynchronize());
  }else{
    deviceReduceKernel<false><<<blocks, threads, smemSize>>>(in, device_out, N);
    gpuErrchk(cudaDeviceSynchronize());
  }

  float *h_odata = (float *) malloc(blocks*sizeof(float));
  float sum = 0.0;

  gpuErrchk(cudaMemcpy(h_odata, device_out, blocks * sizeof(float),cudaMemcpyDeviceToHost));
  for (int i=0; i<blocks; i++)
  {
    sum += h_odata[i];
  }
  cudaFree(device_out);
  free(h_odata);
	return sum;
}

__global__ void hermitianSymmetry(float *Ux, float *Vx, cufftComplex *Vo, float freq, int numVisibilities)
{
  int i = threadIdx.x + blockDim.x * blockIdx.x;

  if (i < numVisibilities){
      if(Ux[i] < 0.0){
        Ux[i] *= -1.0;
        Vx[i] *= -1.0;
        Vo[i].y *= -1.0;
      }
      Ux[i] = (Ux[i] * freq) / LIGHTSPEED;
      Vx[i] = (Vx[i] * freq) / LIGHTSPEED;
  }
}

__global__ void attenuation(float beam_fwhm, float beam_freq, float beam_cutoff, cufftComplex *attenMatrix, float freq, long N, float xobs, float yobs, float DELTAX, float DELTAY)
{

		int j = threadIdx.x + blockDim.x * blockIdx.x;
		int i = threadIdx.y + blockDim.y * blockIdx.y;

    int x0 = xobs;
    int y0 = yobs;
    float x = (j - x0) * DELTAX * RPDEG;
    float y = (i - y0) * DELTAY * RPDEG;

    float arc = sqrtf(x*x+y*y);
    float c = 4.0*logf(2.0);
    float a = (beam_fwhm*beam_freq/(freq*1e-9));
    float r = arc/a;
    float atten = expf(-c*r*r);
    if(arc <= beam_cutoff){
      attenMatrix[N*i+j].x = atten;
      attenMatrix[N*i+j].y = 0.0;
    }else{
      attenMatrix[N*i+j].x = 0.0;
      attenMatrix[N*i+j].y = 0.0;
    }
}



__global__ void total_attenuation(cufftComplex *total_atten, cufftComplex *attenperFreq, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
  int i = threadIdx.y + blockDim.y * blockIdx.y;

  total_atten[N*i+j].x += attenperFreq[N*i+j].x;
  total_atten[N*i+j].y = 0;
}

__global__ void mean_attenuation(cufftComplex *total_atten, int channels, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
  int i = threadIdx.y + blockDim.y * blockIdx.y;

  total_atten[N*i+j].x /= channels;
  total_atten[N*i+j].y = 0;
}

__global__ void noise_image(cufftComplex *noise_image, cufftComplex *weight_image, float difmap_noise, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
  int i = threadIdx.y + blockDim.y * blockIdx.y;

  float noiseval;
  noiseval = sqrtf(1.0/weight_image[N*i+j].x);
  noise_image[N*i+j].x = noiseval;
  noise_image[N*i+j].y = 0;
}

__global__ void weight_image(cufftComplex *weight_image, cufftComplex *total_atten, float difmap_noise, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
  int i = threadIdx.y + blockDim.y * blockIdx.y;

  float atten = total_atten[N*i+j].x;
  weight_image[N*i+j].x += (atten / difmap_noise) * (atten / difmap_noise);
  weight_image[N*i+j].y = 0;
}

__global__ void apply_beam(float beam_fwhm, float beam_freq, float beam_cutoff, cufftComplex *image, cufftComplex *fg_image, long N, float xobs, float yobs, float fg_scale, float freq, float DELTAX, float DELTAY)
{
    int j = threadIdx.x + blockDim.x * blockIdx.x;
    int i = threadIdx.y + blockDim.y * blockIdx.y;


    float dx = DELTAX * 60.0;
    float dy = DELTAY * 60.0;
    float x = (j - xobs) * dx;
    float y = (i - yobs) * dy;
    float arc = RPARCM*sqrtf(x*x+y*y);
    float c = 4.0*logf(2.0);
    float a = (beam_fwhm*beam_freq/(freq*1e-9));
    float r = arc/a;
    float atten = expf(-c*r*r);

    if(arc <= beam_cutoff){
      image[N*i+j].x = fg_image[N*i+j].x * fg_scale * atten;
      image[N*i+j].y = 0.0;
    }else{
      image[N*i+j].x = 0.0;
      image[N*i+j].y = 0.0;
    }


}


/*--------------------------------------------------------------------
 * Phase rotate the visibility data in "image" to refer phase to point
 * (x,y) instead of (0,0).
 * Multiply pixel V(i,j) by exp(-2 pi i (x/ni + y/nj))
 *--------------------------------------------------------------------*/
__global__ void phase_rotate(cufftComplex *data, long M, long N, float xphs, float yphs)
{

		int j = threadIdx.x + blockDim.x * blockIdx.x;
		int i = threadIdx.y + blockDim.y * blockIdx.y;

    float u,v, phase, c, s, re, im;
    float du = xphs/(float)M;
    float dv = yphs/(float)N;

    if(j < M/2){
      u = du * j;
    }else{
      u = du * (j-M);
    }

    if(i < N/2){
      v = dv * i;
    }else{
      v = dv * (i-N);
    }

    phase = 2.0*(u+v);
    #if (__CUDA_ARCH__ >= 300 )
      sincospif(phase, &s, &c);
    #else
      c = cospif(phase);
      s = sinpif(phase);
    #endif
    re = data[N*i+j].x;
    im = data[N*i+j].y;
    data[N*i+j].x = re * c - im * s;
    data[N*i+j].y = re * s + im * c;
}


/*
 * Interpolate in the visibility array to find the visibility at (u,v);
 */
__global__ void vis_mod(cufftComplex *Vm, cufftComplex *Vo, cufftComplex *V, float *Ux, float *Vx, float deltau, float deltav, long numVisibilities, long N)
{
  int i = threadIdx.x + blockDim.x * blockIdx.x;
  long i1, i2, j1, j2;
  float du, dv, u, v;
  float v11, v12, v21, v22;
  float Zreal;
  float Zimag;

  if (i < numVisibilities){

    u = Ux[i]/deltau;
    v = Vx[i]/deltav;

    if (fabsf(u) > (N/2)+0.5 || fabsf(v) > (N/2)+0.5) {
      printf("Error in residual: u,v = %f,%f\n", u, v);
      asm("trap;");
    }

    if(u < 0.0){
      u = N + u;
    }

    if(v < 0.0){
      v = N + v;
    }

    i1 = u;
    i2 = (i1+1)%N;
    du = u - i1;
    j1 = v;
    j2 = (j1+1)%N;
    dv = v - j1;

    if (i1 < 0 || i1 > N || j1 < 0 || j2 > N) {
      printf("Error in residual: u,v = %f,%f, %ld,%ld, %ld,%ld\n", u, v, i1, i2, j1, j2);
      asm("trap;");
    }

    /* Bilinear interpolation: real part */
    v11 = V[N*j1 + i1].x; /* [i1, j1] */
    v12 = V[N*j2 + i1].x; /* [i1, j2] */
    v21 = V[N*j1 + i2].x; /* [i2, j1] */
    v22 = V[N*j2 + i2].x; /* [i2, j2] */
    Zreal = (1-du)*(1-dv)*v11 + (1-du)*dv*v12 + du*(1-dv)*v21 + du*dv*v22;
    /* Bilinear interpolation: imaginary part */
    v11 = V[N*j1 + i1].y; /* [i1, j1] */
    v12 = V[N*j2 + i1].y; /* [i1, j2] */
    v21 = V[N*j1 + i2].y; /* [i2, j1] */
    v22 = V[N*j2 + i2].y; /* [i2, j2] */
    Zimag = (1-du)*(1-dv)*v11 + (1-du)*dv*v12 + du*(1-dv)*v21 + du*dv*v22;

    Vm[i].x = Zreal;
    Vm[i].y = Zimag;

  }

}

__global__ void alphaVectors(float *alpha_num, float *alpha_den, float *w, cufftComplex *Vm, cufftComplex *Vo, long numVisibilities){
  int i = threadIdx.x + blockDim.x * blockIdx.x;
  if (i < numVisibilities){
    alpha_num[i] = w[i] * ((Vo[i].x * Vm[i].x) + (Vo[i].y * Vm[i].y));
    alpha_den[i] = w[i] * ((Vm[i].x * Vm[i].x) + (Vm[i].y * Vm[i].y));
  }

}

__global__ void residual(cufftComplex *Vr, cufftComplex *Vm, cufftComplex *Vo, long numVisibilities){
  int i = threadIdx.x + blockDim.x * blockIdx.x;
  if (i < numVisibilities){
    Vr[i].x = Vm[i].x - Vo[i].x;
    Vr[i].y = Vm[i].y - Vo[i].y;
  }
}




__global__ void residual_XCORR(cufftComplex *Vr, cufftComplex *Vm, cufftComplex *Vo, float alpha, long numVisibilities)
{
  int i = threadIdx.x + blockDim.x * blockIdx.x;
  if (i < numVisibilities){
    Vm[i].x *= alpha;
    Vm[i].y *= alpha;
    Vr[i].x = Vm[i].x - Vo[i].x;
    Vr[i].y = Vm[i].y - Vo[i].y;
  }
}

__global__ void clipWNoise(cufftComplex *fg_image, cufftComplex *noise, cufftComplex *I, long N, float noise_cut, float MINPIX)
{
	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;


  if(noise[N*i+j].x > noise_cut){
    I[N*i+j].x = MINPIX;
  }

  fg_image[N*i+j].x = I[N*i+j].x;
  fg_image[N*i+j].y = 0;
}


__global__ void getGandDGG(float *gg, float *dgg, float *xi, float *g, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  gg[N*i+j] = g[N*i+j] * g[N*i+j];
  dgg[N*i+j] = (xi[N*i+j] + g[N*i+j]) * xi[N*i+j];
}

__global__ void clip(cufftComplex *I, long N, float MINPIX)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  if(I[N*i+j].x < MINPIX && MINPIX >= 0.0){
      I[N*i+j].x = MINPIX;
  }
  I[N*i+j].y = 0;
}

__global__ void newP(cufftComplex *p, float *xi, float xmin, float MINPIX, long N)
{
	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  xi[N*i+j] *= xmin;
  if(p[N*i+j].x + xi[N*i+j] > MINPIX){
    p[N*i+j].x += xi[N*i+j];
  }else{
    p[N*i+j].x = MINPIX;
    xi[N*i+j] = 0.0;
  }
  p[N*i+j].y = 0.0;
}

__global__ void newPNoPositivity(cufftComplex *p, float *xi, float xmin, long N)
{
	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  xi[N*i+j] *= xmin;
  p[N*i+j].x += xi[N*i+j];
  p[N*i+j].y = 0.0;
}

__global__ void evaluateXt(cufftComplex *xt, cufftComplex *pcom, float *xicom, float x, float MINPIX, long N)
{
	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  if(pcom[N*i+j].x + x * xicom[N*i+j] > MINPIX){
    xt[N*i+j].x = pcom[N*i+j].x + x * xicom[N*i+j];
  }else{
      xt[N*i+j].x = MINPIX;
  }
  xt[N*i+j].y = 0.0;
}

__global__ void evaluateXtNoPositivity(cufftComplex *xt, cufftComplex *pcom, float *xicom, float x, long N)
{
	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  xt[N*i+j].x = pcom[N*i+j].x + x * xicom[N*i+j];
  xt[N*i+j].y = 0.0;
}


__global__ void chi2Vector(float *chi2, cufftComplex *Vr, float *w, long numVisibilities)
{
	int i = threadIdx.x + blockDim.x * blockIdx.x;

	if (i < numVisibilities){
		chi2[i] =  w[i] * ((Vr[i].x * Vr[i].x) + (Vr[i].y * Vr[i].y));
	}

}

__global__ void SVector(float *S, cufftComplex *noise, cufftComplex *I, long N, float noise_cut, float MINPIX)
{
	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  float entropy = 0.0;
  if(noise[N*i+j].x <= noise_cut){
    entropy = I[N*i+j].x * logf(I[N*i+j].x / MINPIX);
  }

  S[N*i+j] = entropy;
}

__global__ void QVector(float *S, cufftComplex *noise, cufftComplex *I, long N, float noise_cut, float MINPIX)
{
	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  float entropy = 0.0;
  if(noise[N*i+j].x <= noise_cut){
    if((i>0 && i<N) && (j>0 && j<N)){
      entropy = (I[N*i+j].x - I[N*i+(j-1)].x) * (I[N*i+j].x - I[N*i+(j-1)].x) + (I[N*i+j].x - I[N*i+(j+1)].x) * (I[N*i+j].x - I[N*i+(j+1)].x) + (I[N*i+j].x - I[N*(i-1)+j].x) * (I[N*i+j].x - I[N*(i-1)+j].x) + (I[N*i+j].x - I[N*(i+1)+j].x) * (I[N*i+j].x - I[N*(i+1)+j].x);
      entropy /= 2;
    }else{
      entropy = I[N*i+j].x;
    }
  }

  S[N*i+j] = entropy;
}

__global__ void TVVector(float *TV, cufftComplex *noise, cufftComplex *I, long N, float noise_cut, float MINPIX)
{
	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  float tv = 0.0;
  if(noise[N*i+j].x <= noise_cut){
    if(i!= N-1 || j!=N-1){
      float dx = I[N*i+(j+1)].x - I[N*i+j].x;
      float dy = I[N*(i+1)+j].x - I[N*i+j].x;
      tv = sqrtf((dx * dx) + (dy * dy));
    }else{
      tv = 0;
    }
  }

  TV[N*i+j] = tv;
}
__global__ void searchDirection(float *g, float *xi, float *h, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  g[N*i+j] = -xi[N*i+j];
  xi[N*i+j] = h[N*i+j] = g[N*i+j];
}

__global__ void newXi(float *g, float *xi, float *h, float gam, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  g[N*i+j] = -xi[N*i+j];
  xi[N*i+j] = h[N*i+j] = g[N*i+j] + gam * h[N*i+j];
}

__global__ void restartDPhi(float *dphi, float *dChi2, float *dH, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
  int i = threadIdx.y + blockDim.y * blockIdx.y;

  dphi[N*i+j] = 0.0;
  dChi2[N*i+j] = 0.0;
  dH[N*i+j] = 0.0;

}

__global__ void DS(float *dH, cufftComplex *I, cufftComplex *noise, float noise_cut, float lambda, float MINPIX, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  if(noise[N*i+j].x <= noise_cut){
    dH[N*i+j] = lambda * (logf(I[N*i+j].x / MINPIX) + 1.0);
  }
}

__global__ void DQ(float *dQ, cufftComplex *I, cufftComplex *noise, float noise_cut, float lambda, float MINPIX, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  if(noise[N*i+j].x <= noise_cut){
    if((i>0 && i<N) && (j>0 && j<N)){
    dQ[N*i+j] = (I[N*i+j].x - I[N*i+(j-1)].x) + (I[N*i+j].x - I[N*i+(j+1)].x) + (I[N*i+j].x - I[N*(i-1)+j].x)  + (I[N*i+j].x - I[N*(i+1)+j].x);
  }else{
    dQ[N*i+j] = I[N*i+j].x;
    }
    dQ[N*i+j] *= lambda;
  }
}

__global__ void DTV(float *dTV, cufftComplex *I, cufftComplex *noise, float noise_cut, float lambda, float MINPIX, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  float dtv = 0.0;
  float num = 0.0;
  float den = 0.0;
  if(noise[N*i+j].x <= noise_cut){
    if(i!= N-1 || j!=N-1){
      float a = I[N*i+(j+1)].x;
      float b = I[N*(i+1)+j].x;
      float y = I[N*i+j].x;
      float num = -a-b+(2*y);
      float den = (a*a) - 2*y*(a+b) + (b*b) + 2*(y*y);
      if(den <= 0){
        dtv = 0;
      }else{
        dtv = num/sqrtf(den);
      }
    }else{
      dtv = 0;
    }
    dTV[N*i+j] = lambda * dtv;
  }
}

__global__ void DChi2(cufftComplex *noise, cufftComplex *atten, float *dChi2, cufftComplex *Vr, float *U, float *V, float *w, long N, long numVisibilities, float fg_scale, float noise_cut, float xobs, float yobs, float DELTAX, float DELTAY)
{

	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  int x0 = xobs;
  int y0 = yobs;
  float x = (j - x0) * DELTAX * RPDEG;
  float y = (i - y0) * DELTAY * RPDEG;

	float Ukv, Vkv, cosk, sink;

  float dchi2 = 0.0;
  if(noise[N*i+j].x <= noise_cut){
  	for(int v=0; v<numVisibilities; v++){
      Ukv = x * U[v];
  		Vkv = y * V[v];
      #if (__CUDA_ARCH__ >= 300 )
        sincospif(2.0*(Ukv+Vkv), &sink, &cosk);
      #else
        cosk = cospif(2.0*(Ukv+Vkv));
        sink = sinpif(2.0*(Ukv+Vkv));
      #endif
      dchi2 += w[v]*((Vr[v].x * cosk) - (Vr[v].y * sink));
  	}

  dchi2 *= fg_scale * atten[N*i+j].x;
  dChi2[N*i+j] = dchi2;
  }
}


__global__ void DChi2_XCORR(cufftComplex *noise, cufftComplex *atten, float *dChi2, cufftComplex *Vr, float *U, float *V, float *w, long N, long numVisibilities, float fg_scale, float noise_cut, float xobs, float yobs, float alpha, float DELTAX, float DELTAY)
{

	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  int x0 = xobs;
  int y0 = yobs;
  float x = (j - x0) * DELTAX * RPDEG;
  float y = (i - y0) * DELTAY * RPDEG;

	float Ukv, Vkv, cosk, sink;

  float dchi2 = 0.0;
  if(noise[N*i+j].x <= noise_cut){
  	for(int v=0; v<numVisibilities; v++){
      Ukv = x * U[v];
  		Vkv = y * V[v];
      #if (__CUDA_ARCH__ >= 300 )
        sincospif(2.0*(Ukv+Vkv), &sink, &cosk);
      #else
        cosk = cospif(2.0*(Ukv+Vkv));
        sink = sinpif(2.0*(Ukv+Vkv));
      #endif
      dchi2 += w[v]*((Vr[v].x * cosk) - (Vr[v].y * sink));
  	}

  dchi2 *= alpha * fg_scale * atten[N*i+j].x;
  dChi2[N*i+j] = dchi2;
  }
}

__global__ void DChi2_total(float *dchi2_total, float *dchi2, long N)
{

	int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  dchi2_total[N*i+j] += dchi2[N*i+j];
}

__global__ void DPhi(float *dphi, float *dchi2, float *dH, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  dphi[N*i+j] = dchi2[N*i+j] + dH[N*i+j];
}

__global__ void substraction(float *x, cufftComplex *xc, float *gc, float lambda, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  x[N*i+j] = xc[N*i+j].x - lambda*gc[N*i+j];
}

__global__ void projection(float *px, float *x, float MINPIX, long N){

  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;


  if(INFINITY < x[N*i+j]){
    px[N*i+j] = INFINITY;
  }else{
    px[N*i+j] = x[N*i+j];
  }

  if(MINPIX > px[N*i+j]){
    px[N*i+j] = MINPIX;
  }else{
    px[N*i+j] = px[N*i+j];
  }
}

__global__ void normVectorCalculation(float *normVector, float *gc, long N){
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  normVector[N*i+j] = gc[N*i+j] * gc[N*i+j];
}

__global__ void copyImage(cufftComplex *p, float *device_xt, long N)
{
  int j = threadIdx.x + blockDim.x * blockIdx.x;
	int i = threadIdx.y + blockDim.y * blockIdx.y;

  p[N*i+j].x = device_xt[N*i+j];
}


__host__ float chiCuadrado(cufftComplex *I)
{
  if(num_gpus == 1){
    cudaSetDevice(selected);
  }else{
    cudaSetDevice(firstgpu);
  }

  float resultPhi = 0.0;
  float resultchi2  = 0.0;
  float resultS  = 0.0;

  if(clip_flag){
    clip<<<numBlocksNN, threadsPerBlockNN>>>(I, N, MINPIX);
    gpuErrchk(cudaDeviceSynchronize());
  }

  clipWNoise<<<numBlocksNN, threadsPerBlockNN>>>(device_fg_image, device_noise_image, I, N, noise_cut, MINPIX);
  gpuErrchk(cudaDeviceSynchronize());


  if(iter>0 && lambda!=0.0){
    switch(reg_term){
      case 0:
        SVector<<<numBlocksNN, threadsPerBlockNN>>>(device_S, device_noise_image, device_fg_image, N, noise_cut, MINPIX);
        gpuErrchk(cudaDeviceSynchronize());
        break;
      case 1:
        QVector<<<numBlocksNN, threadsPerBlockNN>>>(device_S, device_noise_image, device_fg_image, N, noise_cut, MINPIX);
        gpuErrchk(cudaDeviceSynchronize());
        break;
      case 2:
        TVVector<<<numBlocksNN, threadsPerBlockNN>>>(device_S, device_noise_image, device_fg_image, N, noise_cut, MINPIX);
        gpuErrchk(cudaDeviceSynchronize());
        break;
      default:
        printf("Selected prior is not defined\n");
        goToError();
        break;
    }
  }

  if(num_gpus == 1){
    cudaSetDevice(selected);
    for(int f=0; f<nfields;f++){
      for(int i=0; i<data.total_frequencies;i++){

        if(fields[f].numVisibilitiesPerFreq[i] != 0){
        	apply_beam<<<numBlocksNN, threadsPerBlockNN>>>(beam_fwhm, beam_freq, beam_cutoff, device_image, device_fg_image, N, fields[f].global_xobs, fields[f].global_yobs, fg_scale, fields[f].visibilities[i].freq, DELTAX, DELTAY);
        	gpuErrchk(cudaDeviceSynchronize());

        	//FFT 2D
        	if ((cufftExecC2C(plan1GPU, (cufftComplex*)device_image, (cufftComplex*)device_V, CUFFT_FORWARD)) != CUFFT_SUCCESS) {
        		printf("CUFFT exec error\n");
        		goToError();
        	}
        	gpuErrchk(cudaDeviceSynchronize());

          //PHASE_ROTATE
          phase_rotate<<<numBlocksNN, threadsPerBlockNN>>>(device_V, M, N, fields[f].global_xobs, fields[f].global_yobs);
        	gpuErrchk(cudaDeviceSynchronize());

          //RESIDUAL CALCULATION
          vis_mod<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_visibilities[i].Vm, fields[f].device_visibilities[i].Vo, device_V, fields[f].device_visibilities[i].u, fields[f].device_visibilities[i].v, deltau, deltav, fields[f].numVisibilitiesPerFreq[i], N);
        	gpuErrchk(cudaDeviceSynchronize());

          if(xcorr_flag){
            float alpha_num = 0.0;
            float alpha_den = 0.0;
            alphaVectors<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_vars[i].alpha_num, fields[f].device_vars[i].alpha_den, fields[f].device_visibilities[i].weight, fields[f].device_visibilities[i].Vm, fields[f].device_visibilities[i].Vo, fields[f].numVisibilitiesPerFreq[i]);
            gpuErrchk(cudaDeviceSynchronize());

            alpha_num = deviceReduce(fields[f].device_vars[i].alpha_num, fields[f].numVisibilitiesPerFreq[i]);

            alpha_den = deviceReduce(fields[f].device_vars[i].alpha_den, fields[f].numVisibilitiesPerFreq[i]);

            if(alpha_den > 0.0){
              fields[f].device_vars[i].alpha = alpha_num/alpha_den;
            }else{
              fields[f].device_vars[i].alpha = 1.0;
            }
            residual_XCORR<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].Vm, fields[f].device_visibilities[i].Vo, fields[f].device_vars[i].alpha, fields[f].numVisibilitiesPerFreq[i]);
            gpuErrchk(cudaDeviceSynchronize());
          }else{
            residual<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].Vm, fields[f].device_visibilities[i].Vo, fields[f].numVisibilitiesPerFreq[i]);
            gpuErrchk(cudaDeviceSynchronize());
          }

        	////chi 2 VECTOR
        	chi2Vector<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_vars[i].chi2, fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].weight, fields[f].numVisibilitiesPerFreq[i]);
        	gpuErrchk(cudaDeviceSynchronize());

        	//REDUCTIONS
        	//chi2
        	resultchi2  += deviceReduce(fields[f].device_vars[i].chi2, fields[f].numVisibilitiesPerFreq[i]);
        }
      }
    }
  }else{
    for(int f=0; f<nfields; f++){
      #pragma omp parallel for schedule(static,1)
      for (int i = 0; i < data.total_frequencies; i++)
  		{
        float result = 0.0;
        unsigned int j = omp_get_thread_num();
  			//unsigned int num_cpu_threads = omp_get_num_threads();
  			// set and check the CUDA device for this CPU thread
  			int gpu_id = -1;
  			cudaSetDevice((i%num_gpus) + firstgpu);   // "% num_gpus" allows more CPU threads than GPU devices
  			cudaGetDevice(&gpu_id);
        if(fields[f].numVisibilitiesPerFreq[i] != 0){
        	apply_beam<<<numBlocksNN, threadsPerBlockNN>>>(beam_fwhm, beam_freq, beam_cutoff, fields[f].device_vars[i].device_image, device_fg_image, N, fields[f].global_xobs, fields[f].global_yobs, fg_scale, fields[f].visibilities[i].freq, DELTAX, DELTAY);
        	gpuErrchk(cudaDeviceSynchronize());


        	//FFT 2D
        	if ((cufftExecC2C(fields[f].device_vars[i].plan, (cufftComplex*)fields[f].device_vars[i].device_image, (cufftComplex*)fields[f].device_vars[i].device_V, CUFFT_FORWARD)) != CUFFT_SUCCESS) {
        		printf("CUFFT exec error\n");
        		//return -1 ;
        		goToError();
        	}
        	gpuErrchk(cudaDeviceSynchronize());

          //PHASE_ROTATE
          phase_rotate<<<numBlocksNN, threadsPerBlockNN>>>(fields[f].device_vars[i].device_V, M, N, fields[f].global_xobs, fields[f].global_yobs);
        	gpuErrchk(cudaDeviceSynchronize());

          //RESIDUAL CALCULATION
          vis_mod<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_visibilities[i].Vm, fields[f].device_visibilities[i].Vo, fields[f].device_vars[i].device_V, fields[f].device_visibilities[i].u, fields[f].device_visibilities[i].v, deltau, deltav, fields[f].numVisibilitiesPerFreq[i], N);
        	gpuErrchk(cudaDeviceSynchronize());


          if(xcorr_flag){
            float alpha_num = 0.0;
            float alpha_den = 0.0;
            alphaVectors<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_vars[i].alpha_num, fields[f].device_vars[i].alpha_den, fields[f].device_visibilities[i].weight, fields[f].device_visibilities[i].Vm, fields[f].device_visibilities[i].Vo, fields[f].numVisibilitiesPerFreq[i]);
            gpuErrchk(cudaDeviceSynchronize());

            alpha_num = deviceReduce(fields[f].device_vars[i].alpha_num, fields[f].numVisibilitiesPerFreq[i]);

            alpha_den = deviceReduce(fields[f].device_vars[i].alpha_den, fields[f].numVisibilitiesPerFreq[i]);

            if(alpha_den > 0.0){
              fields[f].device_vars[i].alpha = alpha_num/alpha_den;
            }else{
              fields[f].device_vars[i].alpha = 1.0;
            }

            residual_XCORR<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].Vm, fields[f].device_visibilities[i].Vo, fields[f].device_vars[i].alpha, fields[f].numVisibilitiesPerFreq[i]);
            gpuErrchk(cudaDeviceSynchronize());
          }else{
            residual<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].Vm, fields[f].device_visibilities[i].Vo, fields[f].numVisibilitiesPerFreq[i]);
            gpuErrchk(cudaDeviceSynchronize());
          }


        	////chi2 VECTOR
        	chi2Vector<<<fields[f].visibilities[i].numBlocksUV, fields[f].visibilities[i].threadsPerBlockUV>>>(fields[f].device_vars[i].chi2, fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].weight, fields[f].numVisibilitiesPerFreq[i]);
        	gpuErrchk(cudaDeviceSynchronize());


          result = deviceReduce(fields[f].device_vars[i].chi2, fields[f].numVisibilitiesPerFreq[i]);
        	//REDUCTIONS
        	//chi2
          #pragma omp critical
          {
            resultchi2  += result;
          }
        }
      }
    }
  }
  if(num_gpus == 1){
    cudaSetDevice(selected);
  }else{
    cudaSetDevice(firstgpu);
  }
  resultS  = deviceReduce(device_S, M*N);
  resultPhi = (0.5 * resultchi2) + (lambda * resultS);

  final_chi2 = resultchi2;
  final_S = resultS;
  /*printf("chi2 value = %.5f\n", resultchi2);
  printf("S value = %.5f\n", resultS);
  printf("(1/2) * chi2 value = %.5f\n", 0.5*resultchi2);
  printf("lambda * S value = %.5f\n", lambda*resultS);
  printf("Phi value = %.5f\n\n", resultPhi);*/

  return resultPhi;
}



__host__ void dchiCuadrado(cufftComplex *I, float *dxi2)
{

  if(num_gpus == 1){
    cudaSetDevice(selected);
  }else{
    cudaSetDevice(firstgpu);
  }

  if(clip_flag){
    clip<<<numBlocksNN, threadsPerBlockNN>>>(I, N, MINPIX);
    gpuErrchk(cudaDeviceSynchronize());
  }

  restartDPhi<<<numBlocksNN, threadsPerBlockNN>>>(device_dphi, device_dchi2_total, device_dS, N);
  gpuErrchk(cudaDeviceSynchronize());

  toFitsFloat(I, iter, M, N, 1);

  if(iter>0 && lambda!=0.0){
    switch(reg_term){
      case 0:
        DS<<<numBlocksNN, threadsPerBlockNN>>>(device_dS, I, device_noise_image, noise_cut, lambda, MINPIX, N);
        gpuErrchk(cudaDeviceSynchronize());
        break;
      case 1:
        DQ<<<numBlocksNN, threadsPerBlockNN>>>(device_dS, I, device_noise_image, noise_cut, lambda, MINPIX, N);
        gpuErrchk(cudaDeviceSynchronize());
        break;
      case 2:
        DTV<<<numBlocksNN, threadsPerBlockNN>>>(device_dS, I, device_noise_image, noise_cut, lambda, MINPIX, N);
        gpuErrchk(cudaDeviceSynchronize());
        break;
      default:
        printf("Selected prior is not defined\n");
        goToError();
        break;
    }
  }

  if(num_gpus == 1){
    cudaSetDevice(selected);
    for(int f=0; f<nfields; f++){
      for(int i=0; i<data.total_frequencies;i++){
        if(fields[f].numVisibilitiesPerFreq[i] != 0){
            if(xcorr_flag){
              DChi2_XCORR<<<numBlocksNN, threadsPerBlockNN>>>(device_noise_image, fields[f].device_vars[i].atten, fields[f].device_vars[i].dchi2, fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].u, fields[f].device_visibilities[i].v, fields[f].device_visibilities[i].weight, N, fields[f].numVisibilitiesPerFreq[i], fg_scale, noise_cut, fields[f].global_xobs, fields[f].global_yobs, fields[f].device_vars[i].alpha, DELTAX, DELTAY);
            	gpuErrchk(cudaDeviceSynchronize());
            }else{
              DChi2<<<numBlocksNN, threadsPerBlockNN>>>(device_noise_image, fields[f].device_vars[i].atten, fields[f].device_vars[i].dchi2, fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].u, fields[f].device_visibilities[i].v, fields[f].device_visibilities[i].weight, N, fields[f].numVisibilitiesPerFreq[i], fg_scale, noise_cut, fields[f].global_xobs, fields[f].global_yobs, DELTAX, DELTAY);
            	gpuErrchk(cudaDeviceSynchronize());
            }
            DChi2_total<<<numBlocksNN, threadsPerBlockNN>>>(device_dchi2_total, fields[f].device_vars[i].dchi2, N);
          	gpuErrchk(cudaDeviceSynchronize());
        }
      }
    }
  }else{
    for(int f=0;f<nfields;f++){
      #pragma omp parallel for schedule(static,1)
      for (int i = 0; i < data.total_frequencies; i++)
      {
        unsigned int j = omp_get_thread_num();
        //unsigned int num_cpu_threads = omp_get_num_threads();
        // set and check the CUDA device for this CPU thread
        int gpu_id = -1;
        cudaSetDevice((i%num_gpus) + firstgpu);   // "% num_gpus" allows more CPU threads than GPU devices
        cudaGetDevice(&gpu_id);
        if(fields[f].numVisibilitiesPerFreq[i] != 0){
          if(xcorr_flag){
            DChi2_XCORR<<<numBlocksNN, threadsPerBlockNN>>>(device_noise_image, fields[f].device_vars[i].atten, fields[f].device_vars[i].dchi2, fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].u, fields[f].device_visibilities[i].v, fields[f].device_visibilities[i].weight, N, fields[f].numVisibilitiesPerFreq[i], fg_scale, noise_cut, fields[f].global_xobs, fields[f].global_yobs, fields[f].device_vars[i].alpha, DELTAX, DELTAY);
            gpuErrchk(cudaDeviceSynchronize());
          }else{
            DChi2<<<numBlocksNN, threadsPerBlockNN>>>(device_noise_image, fields[f].device_vars[i].atten, fields[f].device_vars[i].dchi2, fields[f].device_visibilities[i].Vr, fields[f].device_visibilities[i].u, fields[f].device_visibilities[i].v, fields[f].device_visibilities[i].weight, N, fields[f].numVisibilitiesPerFreq[i], fg_scale, noise_cut, fields[f].global_xobs, fields[f].global_yobs, DELTAX, DELTAY);
            gpuErrchk(cudaDeviceSynchronize());
          }

          #pragma omp critical
          {
            DChi2_total<<<numBlocksNN, threadsPerBlockNN>>>(device_dchi2_total, fields[f].device_vars[i].dchi2, N);
            gpuErrchk(cudaDeviceSynchronize());
          }
        }
      }
    }
  }

  if(num_gpus == 1){
    cudaSetDevice(selected);
  }else{
    cudaSetDevice(firstgpu);
  }

  DPhi<<<numBlocksNN, threadsPerBlockNN>>>(device_dphi, device_dchi2_total, device_dS, N);
  gpuErrchk(cudaDeviceSynchronize());

  gpuErrchk(cudaMemcpy2D(dxi2, sizeof(float), device_dphi, sizeof(float), sizeof(float), M*N, cudaMemcpyDeviceToDevice));

}
